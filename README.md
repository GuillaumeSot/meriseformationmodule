# Conception BDD avec MERISE

## Table des matières

-   [Contexte du projet](#contexte-du-projet)
-   [Définition de l'acronyme MERISE](#définition-de-l-acronyme-merise)
-   [Construit avec](#construit-avec)
-   [Documentation](#documentation)

## Contexte du projet

Les formations sont organisés en modules.

Chaque module est caractérisé par un numéro de module sous forme de Semantic Versionning, un intitulé, un objectif pédagogique, un contenu (textes, images et vidéos), une durée en heures, un ou plusieurs tags et un auteur.

Un module peut faire partie d'une ou plusieurs formations, comme par exemple un pire module "Commandes de base Git" pourrait faire partie d'une pire formation "Frontend Javascript" et "DevOps", voir  plus.

Un module peut contenir un texte et/ou une image et/ou une vidéo.

Les apprenants peuvent s'inscrire à une ou plusieurs formations, ils peuvent choisir de ne pas suivre certains des modules s'ils possèdent déjà, par exemple, les compétences. Autrement dit, ils peuvent arbitrairement valider les modules de leur choix en un clic.

Chaque apprenant est évalué pour chaque module et possède un état de fin de module (OK / KO).

Une formation est considérée comme terminée lorsque tous les modules ont été validés.

Chaque apprenant est caractérisé par un numéro d’inscription unique, un nom, un prénom, une adresse et une date de naissance.

Un formateurs est auteur d'un module pour une formation donnée, chaque formateur est caractérisé par un code, un nom, un prénom.

## Définition de l' "acronyme" MERISE
1. ### Introduction
    MERISE est une méthode d'analyse et de conception des systèmes d'information (SI). Développée en France dans les années 1970, MERISE permet de structurer et de formaliser les étapes nécessaires pour concevoir un système d'information, en passant de l'expression des besoins à la réalisation technique.
    L'acronyme MERISE n'existe pas en tant que signification officielle et spécifique. 

    Dans le livre de référence présentant la méthode Merise, la préface rédigée par Jacques Lesourne introduisait une analogie avec le merisier.

    L’analogie en question: 
    « _qui ne peut porter de beaux fruits que si on lui greffe une branche de cerisier : ainsi en va-t-il des méthodes informatiques bien conçues, qui ne produisent de bons résultats que si la greffe sur l'organisation réussit_ ».

    Même si beaucoup de gens ont voulu y voir un acronyme comme « Méthode d'Étude et de Réalisation Informatique par les Sous-Ensembles », le nom "MERISE" a donc été choisi sans intention de le développer en acronyme.


2. ### Objectif
    La méthode MERISE vise à structurer le développement des systèmes d'information en séparant clairement les niveaux conceptuel, logique et physique. Elle formalise les besoins des utilisateurs pour garantir que le système réponde précisément aux attentes. En utilisant des modèles clairs, MERISE améliore la communication entre les acteurs du projet, assure la qualité et la cohérence des données, et optimise les processus métiers. La méthode facilite également la maintenance et l'évolutivité des systèmes, tout en réduisant les risques et les coûts. Enfin, elle promeut une approche modulaire, favorisant la réutilisation des composants et la flexibilité du système.


    - **Structurer le développement** : Fournir un cadre méthodologique rigoureux pour toutes les phases du développement des systèmes d'information.
    - **Séparation des niveaux de modélisation** : Distinguer clairement les niveaux conceptuel, logique et physique pour traiter indépendamment les aspects conceptuels et techniques.
    - **Formaliser les besoins des utilisateurs** : Recueillir et analyser de manière exhaustive les besoins pour garantir que le système réponde aux attentes.
    - **Améliorer la communication** : Utiliser des modèles clairs et normalisés pour faciliter la communication entre les différents acteurs du projet.
    - **Assurer la qualité des données** : Garantir la cohérence et l'intégrité des informations gérées par le système.
    - **Optimiser les processus** : Analyser et modéliser les processus métiers pour améliorer l'efficacité et la performance.
    - **Faciliter la maintenance et l'évolutivité** : Permettre une gestion plus facile des évolutions et de la maintenance grâce à une documentation structurée.
    - **Réduire les risques et les coûts** : Maîtriser les risques, les coûts et les délais en suivant une approche méthodologique éprouvée.
    - **Promouvoir une approche Modulaire** : Encourager la réutilisation des composants et la flexibilité dans l'évolution du système.

## Construit avec

- **Looping** pour le diagramme MCD/MLD 
- **Google doc** pour les règles de gestion
- **Google sheet** pour le dictionnaire de données

## Documentation

- https://fr.wikipedia.org/wiki/Merise_(informatique)
- https://github.com/HachemiH/formation-cda-bdd/blob/main/module-2-merise
