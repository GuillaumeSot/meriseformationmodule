# Formation

Une formation a :

- un identifiant unique
- un titre
- une description

1. Une formation est créé par un administrateur
2. Une formation possède zero ou plusieurs modules
3. Une formation a pour inscrit zero ou plusieurs apprenants
4. Une formation est considérée comme terminée lorsque tous les modules ont été validés par l'apprenant

# Module

Un module a :

- un numéro Semantic Versionning
- un titre
- un intitulé,
- un objectif pédagogique
- une durée
- un ou plusieurs tags
- un auteur

1. Un module appartient à une ou plusieurs formation
2. Un module contient zero ou plusieurs leçons
3. Un module possède zero ou plusieurs évaluations
4. Un module possède zero ou plusieurs tags
5. Un module possède un auteur

# Leçon

Une Leçon a :

- un identifiant unique
- un titre
- un ou plusieurs contenus
- un auteur
- une date de création

1. Une leçon est créée par un formateur 
2. Une leçon fait partie d'un ou plusieurs modules

# Apprenant

Un apprenant possède :

- un id
- un nom
- un prénom
- une adresse ou plusieurs adresses
- une date de naissance
- une adresse email
- un mot de passe

1. Un apprenant peut suivre zéro ou plusieurs formations
2. Un apprenant peut choisir de suivre ou non les leçons d'un module
3. Un apprenant possède zero ou plusieurs évaluations de module

# Formateur

Un formateur possède :

- un code formateur
- un nom
- un prénom
- une adresse mail
- un mot de passe

1. Un formateur est l'auteur d'un module
2. Un formateur créer des leçons et ajoute du contenu
3. Un formateur peut avoir accès et modifier ses modules comme bon lui semble

# Administrateur

Il a :

- un id unique
- un identifiant administrateur
- un mot de passe administrateur

1. Un administrateur peut créer des formations en y ajoutant des modules
2. Il possède un droit de création , modification et suppression sur toutes les entités de l'application

# Evaluation

Elle a :

- un identifiant unique
- un statut
- une date
- un module
- un apprenant

1. Si le formateur du module a évalué l'élève alors il pourra renseigner le résultat de l'évaluation OK ou KO

# Tag

Il a :

- un identifiant unique 
- un libelle

1. Il est créer par l'administrateur
2. Il appartient à zero ou plusieurs modules

