CREATE TABLE formations(
   id_formation SERIAL,
   titre VARCHAR(100)  NOT NULL,
   description VARCHAR(100)  NOT NULL,
   PRIMARY KEY(id_formation)
);

CREATE TABLE tags(
   id_tag SERIAL,
   libelle_tag VARCHAR(50)  NOT NULL,
   PRIMARY KEY(id_tag)
);

CREATE TABLE adresses(
   id_adresse SERIAL,
   numero_rue INTEGER NOT NULL,
   rue TEXT NOT NULL,
   ville VARCHAR(255)  NOT NULL,
   code_postal INTEGER NOT NULL,
   PRIMARY KEY(id_adresse)
);

CREATE TABLE apprenants(
   id_apprenant SERIAL,
   nom_apprenant VARCHAR(100)  NOT NULL,
   prenom_apprernant VARCHAR(100)  NOT NULL,
   date_naissance TIMESTAMP,
   courriel_apprenant VARCHAR(255)  NOT NULL,
   mot_de_passe_apprenant VARCHAR(64)  NOT NULL,
   PRIMARY KEY(id_apprenant)
);

CREATE TABLE formateurs(
   id_formateur SERIAL,
   nom_formateur VARCHAR(100)  NOT NULL,
   prenom_formateur VARCHAR(100)  NOT NULL,
   courriel_formateur VARCHAR(255)  NOT NULL,
   mot_de_passe_formateur VARCHAR(64)  NOT NULL,
   PRIMARY KEY(id_formateur)
);

CREATE TABLE lecons(
   id_lecon SERIAL,
   titre_lecon VARCHAR(50)  NOT NULL,
   PRIMARY KEY(id_lecon)
);

CREATE TABLE administrateurs(
   id_administrateur SERIAL,
   identifiant_administrateur VARCHAR(20)  NOT NULL,
   mot_de_passe_administrateur VARCHAR(50)  NOT NULL,
   PRIMARY KEY(id_administrateur)
);

CREATE TABLE modules(
   id_module SERIAL,
   titre_module VARCHAR(100)  NOT NULL,
   intitule TEXT NOT NULL,
   objectif_pedagogique TEXT NOT NULL,
   duree TIME,
   id_formateur INTEGER NOT NULL,
   PRIMARY KEY(id_module),
   FOREIGN KEY(id_formateur) REFERENCES formateurs(id_formateur)
);

CREATE TABLE contenus(
   id_contenu SERIAL,
   type VARCHAR(20)  NOT NULL,
   contenu TEXT NOT NULL,
   id_lecon INTEGER NOT NULL,
   PRIMARY KEY(id_contenu),
   FOREIGN KEY(id_lecon) REFERENCES lecons(id_lecon)
);

CREATE TABLE evaluations(
   id_evaluation SERIAL,
   statut VARCHAR(5)  NOT NULL,
   date_evaluation TIMESTAMP NOT NULL,
   id_module INTEGER NOT NULL,
   id_apprenant INTEGER NOT NULL,
   PRIMARY KEY(id_evaluation),
   FOREIGN KEY(id_module) REFERENCES modules(id_module),
   FOREIGN KEY(id_apprenant) REFERENCES apprenants(id_apprenant)
);

CREATE TABLE taguer(
   id_module INTEGER,
   id_tag INTEGER,
   PRIMARY KEY(id_module, id_tag),
   FOREIGN KEY(id_module) REFERENCES modules(id_module),
   FOREIGN KEY(id_tag) REFERENCES tags(id_tag)
);

CREATE TABLE inscrire(
   id_formation INTEGER,
   id_apprenant INTEGER,
   PRIMARY KEY(id_formation, id_apprenant),
   FOREIGN KEY(id_formation) REFERENCES formations(id_formation),
   FOREIGN KEY(id_apprenant) REFERENCES apprenants(id_apprenant)
);

CREATE TABLE comprendre(
   id_formation INTEGER,
   id_module INTEGER,
   PRIMARY KEY(id_formation, id_module),
   FOREIGN KEY(id_formation) REFERENCES formations(id_formation),
   FOREIGN KEY(id_module) REFERENCES modules(id_module)
);

CREATE TABLE resider(
   id_adresse INTEGER,
   id_apprenant INTEGER,
   PRIMARY KEY(id_adresse, id_apprenant),
   FOREIGN KEY(id_adresse) REFERENCES adresses(id_adresse),
   FOREIGN KEY(id_apprenant) REFERENCES apprenants(id_apprenant)
);

CREATE TABLE posseder(
   id_module INTEGER,
   id_lecon INTEGER,
   PRIMARY KEY(id_module, id_lecon),
   FOREIGN KEY(id_module) REFERENCES modules(id_module),
   FOREIGN KEY(id_lecon) REFERENCES lecons(id_lecon)
);
